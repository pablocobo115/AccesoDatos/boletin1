<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Puerto;
use \app\models\Lleva;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1dao() {
        $numero = Yii::$app->db
                ->createCommand('select distinct edad from ciclista')
                ->queryScalar();
                
        $dataProvider = new SqlDataProvider([
            'sql' => 'select distinct edad from ciclista',
            'totalcount'=>$numero,
            'pagination'=>[
                'pagesize' => 5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 DAO",
            "enunciado"=>"listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISCTINCT edad FROM ciclista"
        ]);
    }
    
    public function actionConsulta1arecord() {
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 Active Record",
            "enunciado"=>"listar las edades de los ciclistas sin repetidos",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    public function actionConsulta2dao() {
        $numero = Yii::$app->db
                ->createCommand('select distinct edad from ciclista where nomequipo = "Artiach"')
                ->queryScalar();
                
        $dataProvider = new SqlDataProvider([
            'sql' => 'select distinct edad from ciclista where nomequipo = "Artiach"',
            'totalcount'=>$numero,
            'pagination'=>[
                'pagesize' => 5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 DAO",
            "enunciado"=>"listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
    public function actionConsulta2arecord() {
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 Active Record",
            "enunciado"=>"listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
     public function actionConsulta3dao(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo ="Artiach" or nomequipo = "Amore Vita"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach" OR nomequipo = "Amore Vita"',
            'totalCount' => $numero,
            'pagination' =>[
                'pageSize'=>5
            ] 
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    public function actionConsulta3arecord(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'")->orWhere("nomequipo = 'Amore Vita'"),
            'pagination' =>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    public function actionConsulta4dao(){
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from ciclista where edad < 25 or edad >30')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30',
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);
        //renderizamos lla vista de la consulta
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=> ['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30"
        ]);
    }
    public function actionConsulta4arecord(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select('dorsal')->where('edad < 25')->orWhere('edad >30'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30"
        ]);
    }
    
    public function actionConsulta5dao(){
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from ciclista where nomequipo="Banesto" and edad between 28 and 32')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT dorsal FROM ciclista WHERE nomequipo="Banesto" AND edad BETWEEN 28 AND 32',
            'totalCount' => $numero,
            'pagination' =>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 de DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 28 AND 32"
        ]);
    }
    public function actionConsulta5arecord(){
        $dataProvider = new ActiveDataProvider([
            'query' =>Ciclista::find()->select('dorsal')->where("nomequipo = 'Banesto'")->andWhere("edad between 28 and 32"),
            'pagination'=>[
                'pageSize'=>5
            ]
        ]);
        
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 de Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 28 AND 32"
        ]);
    }
    public function actionConsulta6dao(){
        $numero = Yii::$app->db
                ->createCommand('select count(nombre) from ciclista where length(nombre)>8')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT nombre FROM ciclista WHERE length(nombre)>8',
            'totalCount'=>$numero,
            'pagination'=>[
                'pagesize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 DAO",
            "enunciado"=>"Ind�came el nombre de los ciclistas que el n�mero de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT nombre FROM ciclista WHERE length(nombre)>8"
        ]);
    }
    public function actionConsulta6arecord(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select('nombre')->where("length(nombre)>8"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Ind�came el nombre de los ciclistas que el n�mero de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT nombre FROM ciclista WHERE length(nombre)>8"
        ]);
    }
    public function actionConsulta7dao(){
        $numero = Yii::$app->db
                ->createCommand('select count(nombre) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            //declarar la variable $nombre_mayusculas en el modelo Ciclista
            'sql'=>'SELECT nombre, dorsal, upper(nombre) AS nombre_mayusculas FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal','nombre_mayusculas'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"L�stame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre may�sculas que debe mostrar el nombre en may�sculas",
            "sql"=>"SELECT nombre, dorsal, upper(nombre) AS nombre_mayusculas FROM ciclista"
        ]);
                  
        }
    public function actionConsulta7arecord(){
        
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select(['nombre','dorsal','upper(nombre) AS nombre_mayusculas']),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal','nombre_mayusculas'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"L�stame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre may�sculas que debe mostrar el nombre en may�sculas",
            "sql"=>"SELECT nombre, dorsal, upper(nombre) AS nombre_mayusculas FROM ciclista"
        ]);
    }
    public function actionConsulta8dao(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from lleva where c�digo="MGE"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM lleva WHERE c�digo="MGE"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE maillot='MGE'"
        ]);
                  
        }
    public function actionConsulta8arecord(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Lleva::find()->select('dorsal')->distinct()->where("c�digo='MGE'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE c�digo='MGE'"
        ]);
    }
    public function actionConsulta9dao(){
        $numero = Yii::$app->db
                ->createCommand('select count(nompuerto) from puerto where altura > 1500')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT nompuerto FROM puerto WHERE altura > 1500',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ] 
        ]);
        
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500"
        ]);
    }
    public function actionConsulta9arecord(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()->select('nompuerto')->distinct()->where("altura > 1500"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500"
        ]);
    }
    public function actionConsulta10dao(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from puerto where pendiente > 8 OR altura between 1800 AND 3000')
                ->queryScalar();
                
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000',
            'totalCount'=> $numero,
            'pagination'=>[
                'pageSize'=>5,
            ]    
        ]);
        
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura est� entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000"
        ]);
    }
    public function actionConsulta10arecord(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()->select('dorsal')->distinct()->where("pendiente > 8")->orWhere("altura between 1800 and 3000"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura est� entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000"
        ]);
        
    }
    public function actionConsulta11dao(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from puerto where pendiente > 8 AND altura between 1800 AND 3000')
                ->queryScalar();
                
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000',
            'totalCount'=> $numero,
            'pagination'=>[
                'pageSize'=>5,
            ]    
        ]);
        
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura est� entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000"
        ]);
    }
    public function actionConsulta11arecord(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()->select('dorsal')->distinct()->where("pendiente > 8")->andWhere("altura between 1800 and 3000"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura est� entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000"
        ]);
    }
}
