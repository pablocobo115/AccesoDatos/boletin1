<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Consultas Boletin 1';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas Boletin 1</h1>

        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 1<h3>
                            <p>Listar las edades de los ciclistas sin repetidos</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta1arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta1dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 2</h3>
                            <p>listar las edades de los ciclistas de Artiach</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta2arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta2dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 3</h3>
                            <p>listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta3arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta3dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 4</h3>
                            <p>listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta4arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta4dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 5</h3>
                            <p>listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta5arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta5dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 6</h3>
                            <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta6arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta6dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 7</h3>
                            <p>lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas
</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta7arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta7dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 8</h3>
                            <p> Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta8arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta8dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 9</h3>
                            <p> Listar el nombre de los puertos cuya altura sea mayor de 1500 </p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta9arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta9dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 10</h3>
                            <p> Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta10arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta10dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 11</h3>
                            <p> Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>
                            <p>
                                <?= Html::a('ActiveRecord', ['site/consulta11arecord'], ['class'=>'btn btn-primary'])?>
                                <?= Html::a('DAO', ['site/consulta11dao'], ['class'=>'btn btn-default'])?>
                            </p>
                </div>
            </div>
        </div>
    </div>
</div>
